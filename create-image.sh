#!/usr/bin/env bash
# Unofficial Bash Strict Mode
set -euo pipefail
IFS=$'\n\t'

if [[ $# -lt 3 ]]; then
	echo "Usage: $(basename "$0") BUILD_FOLDER SCRIPT_APP DMG_NAME [--no-symlink] [Additional files...]"
	exit 1
fi

BUILD_FOLDER="$1"; shift
SCRIPT_APP="$1"; shift
DMG_NAME="$1"; shift
DMG_FOLDER="$BUILD_FOLDER/$DMG_NAME"
DMG=

if [[ "${1:-}" == "--no-symlink" ]]; then
	CREATE_SYMLINK=0
	shift
else
	CREATE_SYMLINK=1
fi

if [[ -d "$DMG_FOLDER" ]]; then
	echo "Removing DMG folder"
	rm -Rf "$DMG_FOLDER"
fi
if [[ -f "$BUILD_FOLDER/$DMG_NAME.dmg" ]]; then
	echo "Removing DMG file"
	rm -f "$BUILD_FOLDER/$DMG_NAME.dmg"
fi
echo "Creating DMG folder"
mkdir -p "$DMG_FOLDER"
echo "Copying application"
cp -R "$SCRIPT_APP" "$DMG_FOLDER/"
echo "Copying additional files"
while [[ $# -gt 0 ]]; do
	echo "- copying $1 to DMG-folder"
	cp -R "$1" "$DMG_FOLDER/"
	shift
done
if [[ $CREATE_SYMLINK == "1" ]]; then
	echo "Creating symlink"
	ln -s /Applications "$DMG_FOLDER/Programma's"
fi
echo "Creating disk image"
DMG="$BUILD_FOLDER/${DMG_NAME// /_}.dmg"
if [[ -f "$DMG" ]]; then
	rm -f "$DMG"
fi
hdiutil create -format ULFO -srcfolder "$DMG_FOLDER" "$DMG"
echo "Done"
