use AppleScript version "2.4" -- Yosemite (10.10) or later
use scripting additions

property domain : missing value

set domain to get_plist_item(path to me, "CFBundleIdentifier")

set log_path to "~/Library/Logs/ArchiPunt/"
set log_name to "go-jamf-pro.log"

set ttl to "Jamf Pro migratie script"
set msg to "Met dit script wordt je gemigreerd naar Jamf Pro. 

LET OP: dit kan even (tot een minuut) duren.

Druk op OK om door te gaan: er zal om een wachtwoord gevraagd worden."

try
	display dialog msg buttons {"OK", "Annuleren"} default button 1 cancel button 2 with title ttl with icon note
	
	-- Zodat het log bestand rechten heeft van de gebruiker
	try
		do shell script "mkdir -p " & log_path & "; touch " & log_path & log_name
	on error err_msg number err_nr
		log "Ignore fails :" & err_msg & " (" & err_nr & ")"
	end try
	
	set test_value to get_default("test", "")
	if test_value = "PASS" then
		set het_script to quoted form of get_resource("test.sh", "bin") # deze gaat goed
	else if test_value = "FAIL" then
		set het_script to quoted form of get_resource("test.sh", "bin") & " GAAT_MIS"
	else
		set het_script to quoted form of get_resource("go-jamf-pro.sh", "bin")
	end if
	
	set cmd to {het_script, ">>", log_path & log_name, "2>&1"}
	set AppleScript's text item delimiters to space
	set cmd to cmd as text
	
	set pwd_prompt to "Autoriseer a.u.b. het migreren naar Jamf Pro"
	
	try
		do shell script cmd with prompt pwd_prompt with administrator privileges
		display dialog "De migratie is volledig. Bedankt voor de medewerking." with title ttl with icon note buttons {"OK"}
		
	on error err_msg number err_nr
		if err_nr = -128 then
			# Annuleren bij authoriseren
			error err_msg number err_nr
		end if
		if err_nr = 199 then
			# exit code voor rebooten
			set msg to "Een herstart is noodzakelijk. Volg na het herstarten de instructies voor de apparaatinschrijving."
			display dialog msg buttons {"Annuleren", "Herstart"} cancel button 1 default button 2 with title ttl with icon caution
			do shell script "reboot" with administrator privileges
		else
			set ttl to "De migratie is helaas niet volledig"
			set msg to "We vragen je om de logfile '" & log_name & "' naar 'helpdesk-ict@archipunt.nl' te sturen."
			
			display dialog msg buttons {"Toon in Finder"} with title ttl with icon caution
			do shell script "open " & log_path
		end if
	end try
	
on error number -128 --user cancel
	display dialog "De migratie is geannuleerd." with title ttl with icon stop buttons {"OK"}
end try

on get_default(key_name, default_vaule)
	set cmd to {"defaults", "read", domain, key_name, "2> /dev/null"}
	set AppleScript's text item delimiters to space
	try
		return do shell script (cmd as text)
	on error
		return default_vaule
	end try
end get_default

on get_resource(item_name, item_folder)
	local path_to_me, base_path, base_subpath, item_path
	set path_to_me to path to me as text
	if path_to_me ends with ".scptd:" or path_to_me ends with ".app:" then
		--It's a Bundle
		set base_path to (POSIX path of path_to_me) & "Contents/"
	else
		set base_path to POSIX path of (path_to_me & "::")
	end if
	if item_folder is missing value then
		set base_subpath to "Resources/"
	else
		set base_subpath to "Resources/" & item_folder & "/"
	end if
	set item_path to base_path & base_subpath & item_name
	return item_path
end get_resource


--reads a value from "Info.plist"
on get_plist_item(path_to_me as text, item_name)
	local base_path, item_path
	if path_to_me ends with ".scptd:" or path_to_me ends with ".app:" then
		--It's a Bundle
		set base_path to (POSIX path of path_to_me) & "Contents/"
	else
		set base_path to POSIX path of (path_to_me & "::")
	end if
	set item_path to base_path & "Info.plist"
	set cmd to "defaults read " & quoted form of item_path & space & quoted form of item_name
	set res to do shell script cmd
	return res
end get_plist_item
