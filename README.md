Go Jamf Pro
===========

Application to help users migrate to Jamf Pro, amongst other things.
See [this blog article][blog].

Develop
-------

Checkout the file `.ok` for handy commands (or just use [ok-bash][]). 

Creating app:

* The application can be built with `make build`
* Then notarized via [SD Notary][sd-notary]
* Finally packaged into an diskimage for distribution via `make image`.


[sd-notary]: https://latenightsw.com/category/sd-notary/
[ok-bash]: https://github.com/secretGeek/ok-bash
[blog]: https://blog.zanstra.com/2021/01/11/go-jamf-pro.html
