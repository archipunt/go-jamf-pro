NAME=go-jamf-pro
SRC=$(CURDIR)/$(NAME).applescript
INFO_SRC=$(CURDIR)/Info.plist
BUILD=$(CURDIR)/build
APP_DST=$(BUILD)/$(NAME).app
APP_NOT=$(BUILD)/${NAME} - Notarized/$(NAME).app
RES_SRC=$(CURDIR)/Resources/
APP_VER=$(shell defaults read "$(CURDIR)/Info.plist" "CFBundleVersion")
help:
	@echo "Targets: clean build image."
	@echo
	@echo "app_version: ${APP_VER}"
clean:
	@echo "Cleaning..."
	@if [[ -d "${BUILD}" ]]; then rm -Rf "${BUILD}"; fi
build:
	@echo "Building..."
	@if [[ ! -d "${BUILD}" ]]; then mkdir "${BUILD}"; fi
	@osacompile -x -o "${APP_DST}" "${SRC}"
	@cp "${INFO_SRC}" "${APP_DST}/Contents"
	@rsync -av --exclude='.*' "${RES_SRC}" "${APP_DST}/Contents/Resources"
	@sed -i "" -e 's/SEE_VERSION_IN_apps_info_plist__DO_NOT_EDIT_THIS_STRING/'"${APP_VER}"'/' "${APP_DST}/Contents/Resources/bin/go-jamf-pro.sh"
	@sed -i "" -e 's/SEE_VERSION_IN_apps_info_plist__DO_NOT_EDIT_THIS_STRING/'"${APP_VER}"'/' "${APP_DST}/Contents/Resources/bin/test.sh"
image:
	@echo "Creating disk image from notarization..."
	@./create-image.sh "${BUILD}" "${APP_NOT}" "${NAME}_${APP_VER}" --no-symlink
