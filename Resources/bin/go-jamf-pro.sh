#!/usr/bin/env bash
# Unofficial Bash Strict Mode
set -euo pipefail
IFS=$'\n\t'

# based on: https://github.com/rtrouton/rtrouton_scripts/blob/master/rtrouton_scripts/migrate_ad_mobile_account_to_local_account/MigrateADMobileAccounttoLocalAccount.command
version="SEE_VERSION_IN_apps_info_plist__DO_NOT_EDIT_THIS_STRING"


function root_check {
    if [[ $EUID -ne 0 ]]; then
        >&2 echo "Please run script with 'sudo $(basename $0) $@', exiting..."
        exit 1
    fi
}

# TESTED OK
function remove_LDAPv3 {

    echo "--[ remove_LDAPv3 ]------------------------------------------"
    #oorspronkelijke registratie herstellen:
    #dsconfigldap -vsemgxN -a ok.archipunt.nl -c $(hostname -s)

    the_dirs="$(/usr/bin/dscl localhost -list .)"
    if [[ $the_dirs == *"LDAPv3"* ]]; then
        echo "Registratie Network Account Server/Open Directory/LDAPv3 verwijderen:"
        /usr/sbin/dsconfigldap -r ok.archipunt.nl || /usr/sbin/dsconfigldap -r 10.20.0.10 || /usr/sbin/dsconfigldap -r 10.20.0.13 || echo "Onbekende LDAPv3 server; er is niets verwijderd; exit-code $?."
    else
        echo "Geen LDAP registratie gevonden."
    fi
}

# TESTED OK
function account_check {

    echo "--[ account_check ]------------------------------------------"

    authentication_authority=$(/usr/bin/dscl . -read /Users/$USER AuthenticationAuthority || echo "dscl: AuthenticationAuthority record not found for $USER, exit-code $?")

    if [[ "$authentication_authority" = *"/LDAPv3/"* ]]; then
        if [[ "$authentication_authority" = *";LocalCachedUser;"* ]]; then
            echo "The $USER account is an Open Directory mobile account"
        else
            echo "The $USER account is not an Open Directory mobile account (2)."
            echo "[[[$authentication_authority]]]"
            return 2
        fi
    else
        echo "The $USER account is not an Open Directory account (1)."
        echo "[[[$authentication_authority]]]"
        return 1
    fi
}

# TESTED OK
function remove_account_attributes {

    echo "--[ remove_account_attributes ]------------------------------------------"

    # Migrate password and remove AD-related attributes
    # Remove the account attributes that identify it as an Active Directory mobile account

    /usr/bin/dscl . -delete /Users/$USER cached_groups
    /usr/bin/dscl . -delete /Users/$USER cached_auth_policy #is er niet?
    /usr/bin/dscl . -delete /Users/$USER CopyTimestamp
    /usr/bin/dscl . -delete /Users/$USER AltSecurityIdentities
    /usr/bin/dscl . -delete /Users/$USER OriginalAuthenticationAuthority
    /usr/bin/dscl . -delete /Users/$USER OriginalNodeName
    /usr/bin/dscl . -delete /Users/$USER SMBSID
    /usr/bin/dscl . -delete /Users/$USER AppleMetaRecordName
    # te onderzoeken: accountPolicyData
}

# TESTED OK
function password_migration {

    echo "--[ password_migration ]------------------------------------------"
    # macOS 10.14.4 will remove the the actual ShadowHashData key immediately 
    # if the AuthenticationAuthority array value which references the ShadowHash
    # is removed from the AuthenticationAuthority array. To address this, the
    # existing AuthenticationAuthority array will be modified to remove the Kerberos
    # and LocalCachedUser user values.
 
    # dit werkt alleen voor de ingelogde user
    AuthenticationAuthority=$(/usr/bin/dscl -plist . -read "/Users/$USER" AuthenticationAuthority || echo "FAILED:AuthenticationAuthority(exit-code=$?)")
#       <?xml version="1.0" encoding="UTF-8"?>
#       <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
#       <plist version="1.0">
#       <dict>
#           <key>dsAttrTypeStandard:AuthenticationAuthority</key>
#           <array>
#               <string>;ShadowHash;HASHLIST:&lt;SALTED-SHA512-PBKDF2,SRP-RFC5054-4096-SHA512-PBKDF2,SMB-NT,CRAM-MD5,RECOVERABLE&gt;</string>
#               <string>;LocalCachedUser;/LDAPv3/ok.archipunt.nl:doeke:E5FD5912-6F95-4980-9DF2-B924E0995652</string>
#               <string>;Kerberosv5;;doeke@OK.ARCHIPUNT.NL;OK.ARCHIPUNT.NL;</string>
#               <string>;SecureToken;</string>
#           </array>
#       </dict>
#       </plist>
    if [[ "$AuthenticationAuthority" == *"FAILED:AuthenticationAuthority"* ]]; then
        echo "-> Geen AuthenticationAuthority entry gevonden. Fataal."
        exit 4
    fi

    Kerberosv5=$(echo "${AuthenticationAuthority}" | xmllint --xpath 'string(//string[contains(text(),"Kerberosv5")])' -)
#        ;Kerberosv5;;doeke@OK.ARCHIPUNT.NL;OK.ARCHIPUNT.NL;
    LocalCachedUser=$(echo "${AuthenticationAuthority}" | xmllint --xpath 'string(//string[contains(text(),"LocalCachedUser")])' -)
#        ;LocalCachedUser;/LDAPv3/ok.archipunt.nl:doeke:E5FD5912-6F95-4980-9DF2-B924E0995652
    
    # Remove Kerberosv5 and LocalCachedUser
    if [[ "${Kerberosv5}" ]]; then
        /usr/bin/dscl -plist . -delete /Users/$USER AuthenticationAuthority "${Kerberosv5}"
    else
        echo "-> no Kerberosv5 entry found"
    fi
    
    if [[ "${LocalCachedUser}" ]]; then
        # local user heeft deze (LocalCachedUser) ook, alleen is de waarde anders
        /usr/bin/dscl -plist . -delete /Users/$USER AuthenticationAuthority "${LocalCachedUser}"
    else
        echo "-> no LocalCachedUser entry found"
    fi
}

function account_post_check {

    echo "--[ account_post_check ]------------------------------------------"

    authentication_authority=$(/usr/bin/dscl . -read /Users/$USER AuthenticationAuthority || echo "account_post_check:FAILED for $USER")

    if [[ "$authentication_authority" = *"/LDAPv3/"* || "$authentication_authority" = *";LocalCachedUser;"* ]]; then
        echo "The $USER account still an LDAPv3 account. Fatal."
        echo "[[[$authentication_authority]]]"
        exit 5
    else
        if [[ "$authentication_authority" = "account_post_check:FAILED"* ]]; then
            echo "Er is iets anders mis gegaan. Fatal."
            echo "[[[$authentication_authority]]]"
            exit 6
        fi
    fi

}

#WORDT NIET GEBRUIKT
function remove_network_groups {
    # Nice to have
    # TODO: groep van account halen op basis van getal
    for g_id in $(id -G | sed $'s/ /\t/g'); do 
        if [[ $g -gt 1000 ]]; then 
            echo "Network group: $g"
        fi
    done

}

#WORDT NIET GEBRUIKT
function fix_permissions {
    #TODO: is dit echt nodig?

    echo "--[ fix_permissions ]------------------------------------------"

    homedir=`/usr/bin/dscl . -read /Users/"$USER" NFSHomeDirectory  | awk '{print $2}'`
    if [[ "$homedir" != "" ]]; then
       /bin/echo "Home directory location: $homedir"
       /bin/echo "Updating home folder permissions for the $USER account"
       /usr/sbin/chown -R "$USER" "$homedir"
    fi
    
    # Add user to the staff group on the Mac
    # Is volgens mij altijd zo...
    /bin/echo "Adding $USER to the staff group on this Mac."
    /usr/sbin/dseditgroup -o edit -a "$USER" -t user staff
}


function remove_profiles {

    echo "--[ remove_profiles ]------------------------------------------"
    #profiles list -all
    #profiles list -all -user $USER

    # man profiles.old  #Oude syntax
    # man profiles      #Nieuwe syntax
    profiles remove -verbose -forced -all || echo "profiles-remove failed (exit-code $?)"
    profiles sync || echo "profiles-sync failed with $?"
}

function check_enrollment {
	echo "--[ check_enrollment ]------------------------------------------"
	enroll_status=$(profiles status -type enrollment | grep "Enrolled via DEP:")
	if [[ $enroll_status == "Enrolled via DEP: Yes" ]]; then
		echo "Device enrolled: Yes"
		return 1
	fi
}

function check_expiration {
	echo "--[ check_expiration ]------------------------------------------"
	keychain="/Library/Keychains/apsd.keychain"
	cert=$(/usr/bin/security find-certificate -p -Z $keychain | /usr/bin/openssl x509 -noout -enddate | cut -f2 -d=)
	expiration_date=$(/bin/date -j -f "%b %d %T %Y %Z" "$cert" "+%Y%m%d")
	current_date=$(/bin/date "+%Y%m%d")
	echo "Status: Checking certificate expiration($cert|$expiration_date|$current_date)"
	if [ $expiration_date -lt $current_date ]; then
		echo "Certificate expired: Yes"
		echo "Status: Deleting apsd.keychain"
		rm -f $keychain
		echo "Status: Keychain deleted, restart required"
		exit 199
	fi
}

function enroll_in_jamf_pro {
    echo "--[ enroll_in_jamf_pro ]------------------------------------------"

    profiles renew -type enrollment || echo "profiles-renew mislukt (exit-code $?)"
    #profiles show -type enrollment #deze regel triggert enrollment
}

function show_user_info {
    echo "--[ show_user_info ]------------------------------------------"

	echo "Displaying user and group information for the $USER account:"
	id $USER
}

# int main(void)
echo "===[ $(basename "$0") - d.d. $(date '+%Y-%m-%d %H:%M:%S') - Versie $versie ]=============================================================="

root_check
remove_LDAPv3
if account_check
then
	# Convert mobile account
	remove_account_attributes
	password_migration

	# Refresh Directory Services
	/usr/bin/killall opendirectoryd
	sleep 20

	account_post_check
	#fix_permissions #niet nodig
	#remove_network_groups
fi

remove_profiles

if check_enrollment
then
	check_expiration
	enroll_in_jamf_pro
fi

show_user_info

echo "---[ END OF $(basename "$0") ]-------------------------------------------------------------------------------------------------------------"
