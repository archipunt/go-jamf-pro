#!/usr/bin/env bash
# Unofficial Bash Strict Mode
set -euo pipefail
IFS=$'\n\t'

version="SEE_VERSION_IN_apps_info_plist__DO_NOT_EDIT_THIS_STRING"
echo "Version $version"

if [[ $# -eq 0 ]]; then
    echo "It's going completely swell."
else
    echo "THIS IS NOT GOOD! #fail"
    exit 1
fi
